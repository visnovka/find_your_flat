package com.example.android.find_your_flat.MySearchCriteriaObject;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Karolina on 13.07.2017.
 */

public class SearchCriteria implements Parcelable {
    boolean isBuy; //false = rent, true = buy
    boolean isFlat; //false = house, true = flat
    int minCost;
    int maxCost;
    String location;


    //TODO we need to make SearchCriteria singleton. Now I'm not sure how to do it with Parcelable because there are 2 constructors
    public SearchCriteria() {
        isBuy = true;
        isFlat = true;
        minCost = 0;
        maxCost = 10000000;
        location = "Prague";
    }

    public SearchCriteria(Parcel parcel) {
        this.isBuy = parcel.readByte()==1?true:false;
        this.isFlat = parcel.readByte()==1?true:false;
        this.minCost = parcel.readInt();
        this.maxCost = parcel.readInt();
        this.location = parcel.readString();
    }

    public boolean isBuy() {
        return isBuy;
    }

    public void setBuy(boolean buy) {
        isBuy = buy;
    }

    public boolean isFlat() {
        return isFlat;
    }

    public void setFlat(boolean flat) {
        isFlat = flat;
    }

    public int getMinCost() {
        return minCost;
    }

    public void setMinCost(int minCost) {
        this.minCost = minCost;
    }

    public int getMaxCost() {
        return maxCost;
    }

    public void setMaxCost(int maxCost) {
        this.maxCost = maxCost;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeByte(this.isBuy==true?(byte)1:(byte)0);
        parcel.writeByte(this.isFlat==true?(byte)1:(byte)0);
        parcel.writeInt(this.minCost);
        parcel.writeInt(this.maxCost);
        parcel.writeString(this.location);
    }

    public static final Parcelable.Creator<SearchCriteria> CREATOR = new Parcelable.Creator<SearchCriteria>() {

        public SearchCriteria createFromParcel(Parcel in) {
            return new SearchCriteria(in);
        }

        public SearchCriteria[] newArray(int size) {
            return new SearchCriteria[size];
        }
    };


}
