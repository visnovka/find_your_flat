package com.example.android.find_your_flat;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.ViewAnimator;

import com.example.android.find_your_flat.MySearchCriteriaObject.SearchCriteria;


/**
 * Created by andronov on 21-May-17.
 */

public class Tab1 extends android.support.v4.app.Fragment{
    private SearchCriteria criteria;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {


        Bundle bundle = getArguments();
        if (bundle != null) {
            criteria = bundle.getParcelable("searchCriteria");
            Log.i("Angelina logs: ", "parcelable not null, is buy = "+ criteria.isBuy());
        }
        //SearchCriteria criteria = new SearchCriteria();
        //final SearchCriteria criteria = getArguments().getParcelable("searchCriteria");

        final View inflatedView = inflater.inflate(R.layout.tab1, container, false);
        //initialize animator and images
        final ViewAnimator animator = (ViewAnimator) inflatedView.findViewById(R.id.animator);
        final ImageView flatImg = (ImageView) inflatedView.findViewById(R.id.imgFlat);
        final ImageView houseImg = (ImageView) inflatedView.findViewById(R.id.imgHouse1);


        //initialization of buttons and settting two buttons (Flat and Buy) in default position as "checked"

        final Switch switch1 = (Switch) inflatedView.findViewById(R.id.switchBr);
        if (switch1 != null) {
            switch1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
                    if (isChecked) {
                        // The toggle is enabled

                        criteria.setBuy(false);
                        Log.i("Angelina logs: ", "is Buy is "+ criteria.isBuy());
                    } else {
                        // The toggle is disabled

                        criteria.setBuy(true);
                        Log.i("Angelina logs: ", "is Buy is "+ criteria.isBuy());
                    }
                }
            });
        }
        final Switch switch2 = (Switch) inflatedView.findViewById(R.id.switchFh);
        if (switch2 != null) {
            switch2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked){
                    if (isChecked) {
                        // The toggle is enabled

                        criteria.setFlat(false);
                        Log.i("Angelina logs: ", "isFlat is " + criteria.isFlat());
                    } else {
                        // The toggle is disabled

                        criteria.setFlat(true);
                        Log.i("Angelina logs: ", "isFlat is "+ criteria.isFlat());
                    }
                }
            });
        }





        //animation
        Animation inAnim = new AlphaAnimation(0,1);
        inAnim.setDuration(2000);
        Animation outAnim = new AlphaAnimation(1,0);
        outAnim.setDuration(2000);
        animator.setInAnimation(inAnim);
        animator.setOutAnimation(outAnim);

        /////////////////////////////////////////////////
        //runs without a timer by reposting this handler at the end of the runnable
        final Handler timerHandler = new Handler();
        final Runnable timerRunnable = new Runnable() {
            @Override
            public void run() {
                animator.showNext();
                timerHandler.postDelayed(this, 2000);
                //this -> this links to timerRunnable, so we don't need a loop, this method will repeat untill we stop it.

            }
        };
        timerHandler.postDelayed(timerRunnable,2000);

        /////////////////////////////////////////////////////

        //this is 2nd solution with OnTouch Listener
        /*animator.setOnTouchListener (new ViewAnimator.OnTouchListener(){
            @Override
            public boolean onTouch(View w, MotionEvent event){
                if(event.getAction() == MotionEvent.ACTION_UP){
                    animator.showNext();
                }

                return true;
            }
        }};
         */





        //creating method OnCheckedChanged - if one button is checked, the second one is unchecked.
        /*flatBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    houseBtn.setChecked(false);

                } else {

                    houseBtn.setChecked(true);
                }
            }


        });
        houseBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    flatBtn.setChecked(false);
                }
                else {
                    flatBtn.setChecked(true);
                }
            }
        });
        buyBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){

                    rentBtn.setChecked(false);
                }
                else {
                    rentBtn.setChecked(true);
                }
            }
        });
        rentBtn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    buyBtn.setChecked(false);
                }
                else {
                    buyBtn.setChecked(true);
                }
            }
        });
        */

        return inflatedView;


    }









}
